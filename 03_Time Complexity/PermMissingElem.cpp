// Codility
// PermMissingElem
// time complexity O(n)
// #XOR_method
// ~ solution borrowed
#include <vector>

int solution(std::vector<int> &A) {
  int missing;
  if (A.size() >= 1) {
    int xorComplete = 1;
    for (unsigned int i = 2; i <= A.size() + 1; i++) {
      xorComplete ^= i; // XOR all range elements (1..N+1)
    }
    int xorIncomplete = A.at(0);
    std::vector<int>::const_iterator it;
    for (it = A.begin() + 1; it != A.end(); it++) {
      xorIncomplete ^= *it; // XOR all array elements (1..N)
    }
    missing = xorComplete ^ xorIncomplete;
  }
  else if (A.size() == 0)
    missing = 1;
  return missing;
}
