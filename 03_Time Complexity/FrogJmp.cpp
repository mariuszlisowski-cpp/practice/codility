// Codility
// FrogJmp
// time complexity O(1)
// ~ solution borrowed

#include <cmath>

int solution(int X, int Y, int D) {
   return (int)ceil((Y - X) / (double)D);
}
