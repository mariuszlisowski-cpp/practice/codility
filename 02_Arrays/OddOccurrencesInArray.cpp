// Codility
// Odd Occurrences In Array
// ~ finds the only unpaired element of an odd array
// time complexity O(N) [fast]
// ~ solution borrowed

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

int solution(vector<int> &);

int main() {
  vector<int> V { 9, 3, 9, 3, 9, 7, 9 };
  cout << solution(V) << endl;
  return 0;
}

int solution(vector<int> &A) {
  unordered_set<int> set;
  vector<int>::const_iterator it;
  for (it = A.begin(); it != A.end(); it++) {
    if (set.find(*it) == set.end())
      set.insert(*it);
    else
      set.erase(*it);
  }
  return *(set.begin());
}
