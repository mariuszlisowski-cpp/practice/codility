// Codility
// Cyclic Rotation
// #iterator #vector #at() #begin() #end()
// ~ my solution

#include <iostream>
#include <vector>

using namespace std;

vector<int> rotateRight(vector<int> &, int);
vector<int> rotateLeft(vector<int> &, int);
void showArr(const vector<int> &);

int main() {
  //vector<int> arr;
  vector<int> arr = { 1, 2, 3, 4, 5 };

  cout << "Array rotation..." << endl;

  if (arr.size()) {
    cout << "How many shifts?" << endl << ": ";
    int N;
    cin >> N;

    cout << "Original array:\t";
    showArr(arr);

    cout << "Rotate right (r) or left (l)?" << endl << ": ";
    char ch;
    cin >> ch;
    switch (ch) {
      case 'r': rotateRight(arr, N);
                cout << "Right rotated:\t";
                showArr(arr);
                break;
      case 'l': rotateLeft(arr, N);
                cout << "Left rotated:\t";
                showArr(arr);
                break;
      default:  exit(0);
    }
  }
  else
    cout << "Array's empty!" << endl;

  return 0;
}

vector<int> rotateRight(vector<int> &A, int K) {
  if (A.size() > 1) {
    int last;
    vector<int>::iterator it;
    while (K--) {
      last = *(A.end() - 1);
      for (it = A.end() - 2; it >= A.begin(); it--) {
        *(it + 1) = *it;
      }
      *(A.begin()) = last; // first becomes last
    }
  }
  return A;
}

vector<int> rotateLeft(vector<int> &A, int K) {
  if (A.size() > 1) {
    int first;
    vector<int>::iterator it;
    while (K--) {
      first = *(A.begin());
      for (it = A.begin(); it != A.end() - 1; it++) {
        *it = *(it + 1);
      }
      *(A.end() - 1) = first; // last becomes first
    }
  }
  return A;
}

void showArr(const vector<int> &A) {
  if (A.size()) {
    for (auto i : A)
      cout << i << " ";
    cout << endl;
  }
}
